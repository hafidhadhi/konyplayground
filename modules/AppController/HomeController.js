//Type your code here
var HomeController = {

  init : () => {
    HomeController.setupHeader();
    HomeController.setupListCategory();
    HomeController.setupSearchBar();
    HomeController.initialProcess(); 
    FrmHome.onDeviceBack = HomeController.onDeviceBack;
    FrmHome.onDestroy = HomeController.onDestroy;
  },

  initialProcess : () => {
    FabricClient.init(
      (res) => {
        kony.print('OnSuccess initialize kony fabric client');
        HomeController.populateScreenData();
      },
      (err) => {
        kony.print(`onError initialize kony fabric client \n ${err}`);
      });
  },

  setupHeader : () => {
    var searchIcon  = FrmHome.SearchIcon;
    var backIcon = FrmHome.BackIcon;
    var bodyHome =  FrmHome.BodyHome;
    var searchBar = FrmHome.SearchBarContainer;
    searchBar.setEnabled(false);
    searchIcon.onTouchStart = (source, x, y) => {
      searchBar.setEnabled(true);
      bodyHome.Content.Main.setEnabled(false);
      bodyHome.zIndex = 2;
      bodyHome.animate(Animation.HomeBody.animDef(), Animation.HomeBody.config, {
        "animationStart": () => {
          bodyHome.Content.PopupSearch.setVisibility(true);
          bodyHome.Content.PopupSearch.animate(Animation.Fading.animDef(), Animation.Fading.config);
        }
      });
      searchBar.animate(Animation.Fading.animDef(), Animation.Fading.config);
    };
    backIcon.onTouchStart = (source, x, y) => {
      HomeController.onDeviceBack();
    };
  },

  setupListCategory : () => {
    var segmentListCategory = FrmHome.SegmentCategory;
    var treePosition = HomeModel.currentTreePos;
    var treeHistory = HomeModel.treeHistory;
    var isSubCtgExist = treeHistory[treePosition] && treeHistory[treePosition].subCategories && treeHistory[treePosition].subCategories.length !== 0;
    var cachedData = isSubCtgExist ? treeHistory[treePosition].subCategories : [];
    segmentListCategory.widgetDataMap = {
      MenuTitle: 'name'
    };
    segmentListCategory.setAnimations({
      visible: {
        definition: Animation.ListCategory.animDef(),
        config: Animation.ListCategory.config,
        callbacks: null
      }
    });
    segmentListCategory.onRowClick = (seguiWidget, sectionIndex, rowIndex, selectedState) => {
      HomeController.populateScreenData(segmentListCategory.data[rowIndex]);
    };
    segmentListCategory.setData(cachedData);
  },

  setupSearchBar: () => {
    var container = FrmHome.SearchBarContainer;
    var bodyHome = FrmHome.BodyHome;
    container.CancelButton.onTouchEnd = () => {
      bodyHome.Content.Main.setEnabled(true);
      bodyHome.animate(Animation.HomeBody.animDef(true), Animation.HomeBody.config);
      bodyHome.Content.PopupSearch.animate(Animation.Fading.animDef(true), Animation.Fading.config, {
        "animationEnd": () => {
          kony.print(`AnimateHomeBodyDone`);
          bodyHome.Content.PopupSearch.setVisibility(false);
          bodyHome.zIndex = 1;
        }
      });
      container.animate(Animation.Fading.animDef(true), Animation.Fading.config,{
        "animationEnd": () => {
          container.setEnabled(false);
        }
      });
    };
  },

  refreshTreeLabel : async () => {
    var screenTitle = FrmHome.ScreenTitle;
    var treePosition = HomeModel.currentTreePos;
    var treeHistory = HomeModel.treeHistory;
    var textArray = [];
    treeHistory.forEach((value, i, array, context) => {
      textArray.push(value.label);
    });
    var text = textArray.join(' -> ');
    screenTitle.text = text;    
  },

  refreshListCategory : async () => {
    var listCategory = FrmHome.SegmentCategory;
    var treePosition = HomeModel.currentTreePos;
    var treeHistory = HomeModel.treeHistory;
    var isSubCtgExist = treeHistory[treePosition] && treeHistory[treePosition].subCategories && treeHistory[treePosition].subCategories.length !== 0;
    var subCategories = isSubCtgExist ? treeHistory[treePosition].subCategories : [];
    listCategory.setData(subCategories);
  },

  refreshHeader : async () => {
    var backIcon = FrmHome.BackIcon;
    var treePosition = HomeModel.currentTreePos;
    backIcon.setVisibility(treePosition !== 0);   
  },

  populateScreenData : (params = { id : 'cat00000'}) => {
    HomeModel.getListCategory(
      params,
      (res) => {
        if (res.subCategories && res.subCategories.length !== 0) {
          HomeController.refreshTreeLabel();
          HomeController.refreshListCategory();
          HomeController.refreshHeader();
        } else {
          HomeController.navigateToProduct(params);
        }
      },
      (err) => {}
    );
  },

  onDeviceBack : () => {
    kony.print('OnDeviceBack');
    if (HomeModel.currentTreePos <= 0) {
      kony.application.exit();
    }
    if (!FrmHome.PopupSearch.isVisible) {
      HomeModel.currentTreePos--;
      HomeModel.treeHistory.pop();
      HomeController.refreshTreeLabel();
      HomeController.refreshListCategory();
      HomeController.refreshHeader();
    }
  },

  navigateToProduct : (navParams) => {
    ProductController.init(navParams);
    FrmProduct.show();
  },

  onDestroy : () => {
    HomeModel.currentTreePos = 0;
    HomeModel.treeHistory = [];
  }
};