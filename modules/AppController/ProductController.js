//Type your code here
var ProductController = {
  init : (navParams) => {
    ProductController.setupHeader();
    ProductController.setupListProduct();
    ProductController.initialProcess(navParams);
    FrmProduct.onDestroy = ProductController.onDestroy;
  },

  initialProcess : (navParams) => {
    FrmProduct.ScreenTitle.text = `Category : ${navParams.name}`;
    ProductController.populateScreenData(navParams);
  },

  setupHeader : () => {
    var backIcon = FrmProduct.BackIcon;
    backIcon.onTouchStart = (source, x, y) => {
      FrmHome.show();
      FrmProduct.destroy();
    };
  },

  setupListProduct : () => {
    var segmentListProduct = FrmProduct.SegmentProduct;
    segmentListProduct.widgetDataMap = {
      ProductName: 'productname',
      ProductImage: 'image',
      IsOnSale: 'onsale'
    };
    segmentListProduct.setAnimations({
      visible: {
        definition: Animation.ListCategory.animDef(),
        config: Animation.ListCategory.config,
        callbacks: null
      }
    });
    segmentListProduct.onRowClick = (seguiWidget, sectionIndex, rowIndex, selectedState) => {
    };
    segmentListProduct.setData(ProductModel.productList);
  },

  populateScreenData : (params = { id : 'cat00000'}) => {
    ProductModel.getProductByCategory(
      params,
      (res) => {
        ProductController.refreshListProduct();
      },
      (err) => {

      });
  },

  refreshListProduct : async () => {
    var segmentListProduct = FrmProduct.SegmentProduct;
    var data = ProductModel.productList;
    segmentListProduct.setData(data);
  },
  
  onDestroy : () => {
    ProductModel.productList = [];
  }
};