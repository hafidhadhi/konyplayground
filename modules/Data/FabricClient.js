//Type your code here
var FabricClient = {
  init: async (
    onSuccess = (res) => {}, 
    onError = (err) => {}) => {
    if (kony.net.isNetworkAvailable(constants.NETWORK_TYPE_ANY)) {
      new kony.sdk().init(
        FabricClient.config.appKey, 
        FabricClient.config.appSecret, 
        FabricClient.config.serviceURL,
        onSuccess, 
        onError
      );
    } else {
      kony.print('Network Unavailable');
    }
  },

  config : {
    appKey : '7a892c903519b89626398483992e3509',
    appSecret : '77dd99531a8a0574b786e8d593090c0a',
    serviceURL : 'https://100037747.auth.konycloud.com/appconfig',
  },
};