//Type your code here

var FabricIntegration = {
  BestBuyService : {
    getListCategory : async (
      body = {}, 
      successCallback = (response) => {}, 
      errorCallback = (err) => {}
    ) => {
      var client = kony.sdk.getCurrentInstance();
      var service = client.getIntegrationService('BestBuyService');
      service.invokeOperation(
        'getListCategory', 
        null, 
        body, 
        successCallback, 
        errorCallback, 
        null
      );
    },

    getProductByCategory : async (
      body = {}, 
      successCallback = (response) => {}, 
      errorCallback = (err) => {}
    ) => {
      var client = kony.sdk.getCurrentInstance();
      var service = client.getIntegrationService('BestBuyService');
      service.invokeOperation(
        'getProductByCategory', 
        null, 
        body, 
        successCallback, 
        errorCallback, 
        null
      );
    }
  }
};