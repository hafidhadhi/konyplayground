//Type your code here

var Animation = {
  ListCategory : {
    config : {
      duration: 0.3,
      fillMode: kony.anim.FILL_MODE_FORWARDS
    },
    animDef : () => {
      var transformObject = kony.ui.makeAffineTransform();
      transformObject.translate(300, 0);
      var transformObject2 = kony.ui.makeAffineTransform();
      transformObject2.translate(0, 0);
      animationDef = {
        0: {
          "transform": transformObject
        },
        100: {
          "transform": transformObject2
        }
      };

      return kony.ui.createAnimation(
        animationDef
      );
    }
  },
  HomeBody : {
    config :  {
      "duration": 1,
      "iterationCount": 1,
      "delay": 0,
      "fillMode": kony.anim.FILL_MODE_FORWARDS
    },
    animDef : (reversed = false) => {
      var animDefinition = {
        "0": {
          "top": !reversed ? "0dp" : "56dp",
        },
        "100": {
          "top":  !reversed ? "56dp" : "0dp",
        }
      };
      return kony.ui.createAnimation(animDefinition);
    }
  },
  Fading: {
    config :  {
      "duration": 1,
      "iterationCount": 1,
      "delay": 0,
      "fillMode": kony.anim.FILL_MODE_FORWARDS
    },
    animDef : (reversed = false) => {
      var animDefinition = {
        "0": {
          "opacity": !reversed ? 0 : 1,
          "stepConfig": {
            "timingFunction": kony.anim.EASE
          },
          "rectified": true
        },
        "100": {
          "opacity": !reversed ? 1 : 0,
          "stepConfig": {
            "timingFunction": kony.anim.EASE
          },
          "rectified": true
        },
      };
      return kony.ui.createAnimation(animDefinition);
    }
  }
};