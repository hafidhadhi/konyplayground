//Type your code here

var HomeModel = {
  currentTreePos : 0,
  treeHistory: [], // Contains id which is tree position, label (string), and subCategories (collection of id and name)   
  getListCategory : async (params, onSuccess = (res) => {}, onError = (err) => {}) => {
    FabricIntegration.BestBuyService.getListCategory(
      params, 
      (res) => {
        if (res.subCategories && res.subCategories.length !== 0) {
          if (params.id === 'cat00000') {
            HomeModel.currentTreePos = 0;
            HomeModel.treeHistory.push({
              id : HomeModel.currentTreePos,
              label : 'Home',
              subCategories : res.subCategories
            });
          } else {
            HomeModel.currentTreePos++;
            HomeModel.treeHistory.push({
              id : HomeModel.currentTreePos,
              label : params.name,
              subCategories : res.subCategories
            });
          }
        } else {}
        onSuccess(res);
        kony.print('GeListCategory' + JSON.stringify(res));
      }, 
      (err) => {
        onError(err);
        kony.print('GetListCategory' + err);
      });
  }
};