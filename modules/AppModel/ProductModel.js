//Type your code here
var ProductModel = {
  productList : [],

  anotherFunction : () => {},

  getProductByCategory : async (params = {}, onSuccess = (res) => {}, onError = (err) => {}) => {
    FabricIntegration.BestBuyService.getProductByCategory(
      params, 
      (res) => {
        var data = res.data.map((f, i, array, context) => {
          return {
            productname: array[i].productname,
            image: array[i].image,
            onsale: array[i].onsale === "true" ? 'ON SALE!!!' : undefined // want to hide (setVisibility = false) a widget ? map it to undefined
          };
        });
        ProductModel.productList = data;
        onSuccess(res);
        kony.print('GetProductByCategory' + JSON.stringify(res));
      }, 
      (err) => {
        onError(err);
        kony.print('GetProductByCategory' + err);
      });
  },
};